from setuptools import setup, find_packages
from os import path

package_name = "cfn-py"
version = 1.0

cwd = path.abspath(path.dirname(__file__))
# Get long Description for the Project
with open(path.join(cwd, 'README.md'), 'r') as f:
    long_description = f.read()


setup(
    name=package_name,
    version=version,
    description="Stelligent Mini-Project. Python Package to run Cloudformation Templates",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://github.com/stelligent/miniproject-ARYAL-BISHWASH',
    author='Bishwash Aryal',
    author_email='bishwash.aryal@gmail.com',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python :: 2.7'
        'License :: OSI Approved :: MIT License',
    ],
    include_package_data=True,
    packages=find_packages(),
    install_requires=[
        "PyYAML == 3.13",
        "boto3 == 1.9.0",
        "botocore == 1.12.0",
        "requests == 2.19.1"
    ],
    entry_points={
        'console_scripts': [
            'cfn-py = cfn_py.cfn_py:main'
        ]
    }
)
