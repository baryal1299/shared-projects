import boto3
import botocore


class Auth:
    profile = None

    def __init__(self, profile='default'):
        self.profile = profile

    def get_session(self):
        return boto3.Session(profile_name=self.profile)


