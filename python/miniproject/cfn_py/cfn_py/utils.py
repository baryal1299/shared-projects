import os
import yaml
import json
import requests
import pprint


def parser_file_exists(parser, arg):
    """
    Validate that File Exists and Return Absolute Path.
    :param parser: ArgumentParser
    :param arg: Command Line Argument
    :return: File Path
    """
    if not os.path.exists(arg):
        parser.error("File Not Found. %s" % arg)
    else:
        return os.path.abspath(arg)


def file_string(_path):
    """
    Read file as string
    :param _path: File Path
    :return:
    """
    with open(_path, 'r') as f:
        return f.read()


def load_yaml(yaml_string):
    """
    Converts Yaml String to Python Object
    :param yaml_string: YAML String
    :return: Python Dict
    """
    try:

        # yaml.BaseLoader leave everything as a string
        return yaml.load(yaml_string, Loader=yaml.BaseLoader)
    except yaml.YAMLError as e:
        raise Exception("YAML Error. %s " % str(e))


def load_json(json_string):
    """
    Converts Json String to Python Object
    :param json_string: JSON file handle
    :return: Python Dict
    """
    try:
        return json.loads(json_string)
    except ValueError as e:
        raise Exception("JSON Decode Error. %s " % str(e))


def file_object(_path):
    """
    Get Python Object from file path. Support .json, .yaml and .yml
    :param _path:
    :return:
    """
    _type = _path.split(".")[-1]  # Get Type of Template in reference to file extension
    _type = _type.replace("yml", "yaml")  # Overwrite type to yaml if extension is .yml
    _string = file_string(_path)

    # Converts string in json or yaml to dict
    if _type in ['json']:
        return load_json(_string)
    elif _type in ['yaml']:
        return load_yaml(_string)
    else:
        raise Exception("Unsupported File Format/Extension. Must be .yml, .yaml or .json")


def load_parameters_yaml(yaml_file_path, env):
    """
    Read Parameters from YAML File and Overwrite with Environment Specific Values.
    :param yaml_file_path: Path to Parameters YAML file.
    :param env: Environment as defined in Parameters YAML file.
    :return: Dictionary of Key, Value.
    """
    _params = None
    try:
        with open(yaml_file_path, 'r') as f:
            _params = load_yaml(f.read())
    except Exception as e:
        raise Exception("Error Reading Properties File.\n %s" % e)

    # Raise Exception for Invalid Environment
    if env and not _params['Environments'].get(env, None):
        raise Exception("Unknown Environment. %s" % env)

    # Overwrite Default Params with Environment Specific Parameters
    if env:
        env_specific_params = _params['Environments'][env]
        _params.update(env_specific_params)
        # Set Environment as Parameter
        _params['Environment'] = env

    # Remove Environments Field from the Parameters.
    if 'Environments' in _params:
        del _params['Environments']

    return _params


def validate_urls(urls, verbose=False):
    """
    Get HTTP statuc code for list of urls
    :param verbose: Print Body Content
    :param urls: List of urls
    :return: Dict with Status Code
    """
    status = {}
    if urls:
        print "Validating URLs..."
    for url in urls:
        response = requests.get(url, timeout=10, verify=False)
        status[url] = response.status_code
        print "URL: %s HTTP_STATUS_CODE: %s %s" % (url, response.status_code, response.reason)
        if verbose:
            print response.text
        print
    return status

