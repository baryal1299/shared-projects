# IDENTIFICATION-OF-LOSS-TO-FOLLOW-UP-LTF-FOR-NEWBORN-HEARING-SCREENING
IDENTIFICATION OF LOSS TO FOLLOW UP (LTF) FOR NEWBORN HEARING SCREENING

### Build maven project for FHIR Server API application
Run following maven goals to build a docker image for the project. 

`
$ mvn clean package dockerfile:build
`

Image: ehdi-fhir-ltf/ehdi-fhir-ltf:latest will be created.

### Build docker database image

`
$ docker build -t ehdi-fhir-ltf-db:latest -f ./database/Dockerfile ./database/
`

Image: ehdi-fhir-ltf-db/ehdi-fhir-ltf-db:latest will be created.

### Run the Application
To Run the application using docker-compose use following command

`
$ docker-compose up --build [-d]
`

-d : Will run the containers in detached mode.
If you would like to monitor the execution logs avoid this flag. 
Which will run the container in foreground.

Once all containers are running, Go to:
- http://localhost:8080/ ( Demo Client Application )
- http://localhost:8080/ehdi-fhir-ltf ( FHIR Server )
- localhost:3306 -- Running MySQL instance with pre-existing data loaded.


### Docker-Compose Services

- mysql
    - Image built during execution from database/Dockerfile
    - ./database/hitrack_swizzle.sql is used to import/load initial data
    - ./db_env parameters are used to configure the user, password and database name.
    
- ehdi-fhir-ltf
    - The Image built using "mvn clean package dockerfile:build" , 
    i.e ehdi-fhir-ltf/ehdi-fhir-ltf:latest will be used to run this container.
    - .war file will be deployed to tomcat8.5-jre8 container 
    - The demo-client application will also be deployed as ROOT 
