pipeline {
    agent any

    stages {

        //Define the mvn commands to Build WAR file
        stage('Build'){

            //Define the docker image to use for the stage
            agent{
                docker{
                    image 'maven:3.5.2-alpine'
                }
            }
            steps{
                sh '''mvn clean package'''
                stash includes: '**/target/*.war', name: 'ehdi-fhir-ltf-app'
            }
        }

        //Define the Build and Deploy stage
        stage('Deploy'){
            steps{
                unstash 'ehdi-fhir-ltf-app'
                script{
                    docker.withRegistry('https://build.hdap.gatech.edu'){
                        //Build and push the database image
                        def databaseImage = docker.build("ecf-fhir-ltf-db:2.0","-f ./database/Dockerfile ./database/")
                        databaseImage.push('latest')

                        //Build and push the application image
                        def applicationImage = docker.build("ecf-fhir-ltf-app:2.0", "-f ./Dockerfile --build-arg WAR_FILE=ehdi-fhir-ltf.war .")
                        applicationImage.push('latest')
                    }
                }
            }
        }

        //Define stage to notify rancher
        stage('Notify'){
            steps{
                script{
                    rancher confirm: true, credentialId: 'rancher-server', endpoint: 'http://rancher.hdap.gatech.edu:8080/v2-beta', environmentId: '1a7', environments: '', image: 'build.hdap.gatech.edu/ecf-fhir-ltf-db:latest', ports: '', service: 'EastCoastFhir/ehdi-fhir-ltf-db', timeout: 50
                    rancher confirm: true, credentialId: 'rancher-server', endpoint: 'http://rancher.hdap.gatech.edu:8080/v2-beta', environmentId: '1a7', environments: '', image: 'build.hdap.gatech.edu/ecf-fhir-ltf-app:latest', ports: '', service: 'EastCoastFhir/ehdi-fhir-ltf', timeout: 50
                }
            }
        }

    }
}
