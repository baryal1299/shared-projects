import unittest
import requests
import warnings
from pprint import pprint


class TestAPI(unittest.TestCase):

    def setUp(self):
        self.server = 'http://localhost:8080'
        # self.server = 'https://cs6440-s18-prj46.apps.hdap.gatech.edu'
        warnings.simplefilter('ignore')

    def test_case_6(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?identifier=DD7262CC-CC1D-4FDD-B85E-A6F800E44CFB&_pretty=true')
        self.assertEqual(r.json()['total'], 8)

        self.assertEqual(r.json()['entry'][6]['resource']['id'], 'E694952B-F6C8-47B5-B968-A70B01261D20')
        self.assertEqual(r.json()['entry'][7]['resource']['id'], '713C0E49-A022-488D-8511-A70B01261D22')

        # Use ends with 'Disposition" as proxy for StageDesc == 'Early Intervention'
        self.assertTrue(r.json()['entry'][6]['resource']['valueQuantity']['system'].endswith('Disposition'))
        self.assertTrue(r.json()['entry'][7]['resource']['valueQuantity']['system'].endswith('Disposition'))

    def test_case_7(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?identifier=84D0422C-1C85-46D0-9E70-A6F000F2BACC&_pretty=true')
        self.assertEqual(r.json()['total'], 6)

        self.assertEqual(r.json()['entry'][0]['resource']['id'], '59BF0280-0BA8-4B51-BAC0-A7AD0110CA12')
        self.assertEqual(r.json()['entry'][1]['resource']['id'], '9D1F501E-D06A-4AF3-BC8F-A7AD0110CA15')
        self.assertEqual(r.json()['entry'][2]['resource']['id'], '81170C8F-DDD8-4A30-9CA1-A7AD0110CA14')

        # Check for Refer status
        self.assertEqual(r.json()['entry'][0]['resource']['valueQuantity']['unit'].lower(), 'refer')
        self.assertEqual(r.json()['entry'][1]['resource']['valueQuantity']['unit'].lower(), 'refer')
        self.assertEqual(r.json()['entry'][2]['resource']['valueQuantity']['unit'].lower(), 'refer')

    def test_case_8(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?identifier=AAD4FF54-5640-4EC3-8DEE-A704011D724A&_pretty=true')
        self.assertEqual(r.json()['total'], 0)

    def test_case_10(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Patient?family=&_pretty=true')
        self.assertFalse(r.ok)

    def test_case_11(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Patient?family=Junsable&_pretty=true')
        self.assertEqual(r.json()['total'], 6)

    def test_case_12(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Patient?family=Gatech&_pretty=true')
        self.assertEqual(r.json()['total'], 0)

    def test_case_13(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Patient?family=Fox&given=Cory&_pretty=true')
        self.assertEqual(r.json()['entry'][0]['resource']['id'], 'B36BFF02-BDF8-479E-B757-A70800BBE90F')

    def test_case_14(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Patient?family=GT&given=Tech&_pretty=true')
        self.assertEqual(r.json()['total'], 0)

    def test_case_15(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?identifier=B36BFF02-BDF8-479E-B757-A70800BBE90F&_pretty=true')
        # Check for 3 observations
        self.assertEqual(r.json()['total'], 3)

        # Check observation IDs
        self.assertEqual(r.json()['entry'][0]['resource']['id'], 'C9F1B503-87FC-4796-9CB9-A7AD01117C2F')
        self.assertEqual(r.json()['entry'][1]['resource']['id'], '5A8F006E-C34D-451B-B7EE-A7AD01117C30')
        self.assertEqual(r.json()['entry'][2]['resource']['id'], 'FFE85D48-FD19-40A9-8EEE-A7AD01117C30')

        # Check for pass status
        self.assertEqual(r.json()['entry'][0]['resource']['valueQuantity']['unit'].lower(), 'pass')
        self.assertEqual(r.json()['entry'][1]['resource']['valueQuantity']['unit'].lower(), 'pass')
        self.assertEqual(r.json()['entry'][2]['resource']['valueQuantity']['unit'].lower(), 'pass')

    def test_case_16(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?family=Rowen&given=Cody&_pretty=true')
        self.assertEqual(r.json()['total'], 8)

        self.assertEqual(r.json()['entry'][6]['resource']['id'], 'E694952B-F6C8-47B5-B968-A70B01261D20')
        self.assertEqual(r.json()['entry'][7]['resource']['id'], '713C0E49-A022-488D-8511-A70B01261D22')

        # Use ends with 'Disposition" as proxy for StageDesc == 'Early Intervention'
        self.assertTrue(r.json()['entry'][6]['resource']['valueQuantity']['system'].endswith('Disposition'))
        self.assertTrue(r.json()['entry'][7]['resource']['valueQuantity']['system'].endswith('Disposition'))

    def test_case_17(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?family=Battlebur&given=Benjamin&_pretty=true')
        self.assertEqual(r.json()['total'], 6)

        self.assertEqual(r.json()['entry'][0]['resource']['id'], '59BF0280-0BA8-4B51-BAC0-A7AD0110CA12')
        self.assertEqual(r.json()['entry'][1]['resource']['id'], '9D1F501E-D06A-4AF3-BC8F-A7AD0110CA15')
        self.assertEqual(r.json()['entry'][2]['resource']['id'], '81170C8F-DDD8-4A30-9CA1-A7AD0110CA14')

        # Check for Refer status
        self.assertEqual(r.json()['entry'][0]['resource']['valueQuantity']['unit'].lower(), 'refer')
        self.assertEqual(r.json()['entry'][1]['resource']['valueQuantity']['unit'].lower(), 'refer')
        self.assertEqual(r.json()['entry'][2]['resource']['valueQuantity']['unit'].lower(), 'refer')

    def test_case_18(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?family=GT&given=Tech&_pretty=true')
        self.assertEqual(r.json()['total'], 0)

    def test_case_19(self):
        r = requests.get(self.server + '/ehdi-fhir-ltf/baseDstu3/Observation?identifier=&_pretty=true')
        self.assertFalse(r.ok)


if __name__ == '__main__':
    unittest.main()