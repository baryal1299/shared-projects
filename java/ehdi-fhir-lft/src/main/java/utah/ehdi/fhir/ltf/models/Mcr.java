package utah.ehdi.fhir.ltf.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mcr")
public class Mcr {
    @Id
    private String baby_id;
    private String mcr_id;
    private String is_computed;
    private String MCREar;
    private Date TestDate;
    private String TestType;
    private String UsedMethod;
    private String TestResult;
    private String StageDesc;

    public String getBaby_id() {
        return baby_id;
    }

    public void setBaby_id(String baby_id) {
        this.baby_id = baby_id;
    }

    public String getMcr_id() {
        return mcr_id;
    }

    public void setMcr_id(String mcr_id) {
        this.mcr_id = mcr_id;
    }

    public String getIs_computed() {
        return is_computed;
    }

    public void setIs_computed(String is_computed) {
        this.is_computed = is_computed;
    }

    public String getMCREar() {
        return MCREar;
    }

    public void setMCREar(String MCREar) {
        this.MCREar = MCREar;
    }

    public Date getTestDate() {
        return TestDate;
    }

    public void setTestDate(Date testDate) {
        TestDate = testDate;
    }

    public String getTestType() {
        return TestType;
    }

    public void setTestType(String testType) {
        TestType = testType;
    }

    public String getUsedMethod() {
        return UsedMethod;
    }

    public void setUsedMethod(String usedMethod) {
        UsedMethod = usedMethod;
    }

    public String getTestResult() {
        return TestResult;
    }

    public void setTestResult(String testResult) {
        TestResult = testResult;
    }

    public String getStageDesc() {
        return StageDesc;
    }

    public void setStageDesc(String stageDesc) {
        StageDesc = stageDesc;
    }
}
