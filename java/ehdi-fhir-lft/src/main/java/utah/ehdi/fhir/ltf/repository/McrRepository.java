package utah.ehdi.fhir.ltf.repository;

import utah.ehdi.fhir.ltf.models.Mcr;
import org.springframework.data.repository.CrudRepository;

public interface McrRepository extends CrudRepository<Mcr, String> {

}