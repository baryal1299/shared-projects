package utah.ehdi.fhir.ltf.providers;

import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.dstu3.model.ContactPoint.ContactPointUse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.springframework.jdbc.core.JdbcTemplate;
import org.hl7.fhir.dstu3.model.Address;
import org.hl7.fhir.dstu3.model.ContactPoint;
import org.hl7.fhir.dstu3.model.IdType;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import utah.ehdi.fhir.ltf.FhirServerConfig;

public class RestfulPatientResourceProvider implements IResourceProvider {
	/**
     * The getResourceType method comes from IResourceProvider, and must
     * be overridden to indicate what type of resource this provider
     * supplies.
     */
	    
	private JdbcTemplate jdbcTemplate;
    
    public Class<Patient> getResourceType() {
        return Patient.class;
    }
     
    /**
     * The "@Read" annotation indicates that this method supports the
     * read operation. Read operations should return a single resource
     * instance.
     *
     * @param theId
     *    The read operation takes one parameter, which must be of type
     *    IdType and must be annotated with the "@Read.IdParam" annotation.
     * @return
     *    Returns a resource matching this identifier, or null if none exists.
     */
    
    @Read()
    public Patient getResourceById(@IdParam IdType theId) {
    	
    	String sql = "SELECT * FROM vwDohmpiBaby WHERE id = ?";
    	
    	System.out.println(theId.getValue());
    	Object sqlParams[] = new Object[] { theId.getIdPart() };
    	    	
    	List<Patient> patients = getPatients(sql, sqlParams);
    	
    	if (patients.isEmpty()){
    		return null;
    	}else{
    		return patients.get(0);
    	}
        
    }
 
    /**
     * The "@Search" annotation indicates that this method supports the
     * search operation. You may have many different method annotated with
     * this annotation, to support many different search criteria. This
     * example searches by family name.
     *
     * @param theFamilyName
     *    This operation takes one parameter which is the search criteria. It is
     *    annotated with the "@Required" annotation. This annotation takes one argument,
     *    a string containing the name of the search criteria. The datatype here
     *    is StringParam, but there are other possible parameter types depending on the
     *    specific search criteria.
     * @return
     *    This method returns a list of Patients. This list may contain multiple
     *    matching resources, or it may also be empty.
     */
    
    @Search()
    public List<Patient> searchByName(@RequiredParam(name=Patient.SP_FAMILY) StringParam theFamilyName,
    		 							  @OptionalParam(name=Patient.SP_GIVEN)  StringParam theGivenName) {
    	
    	String familyName = theFamilyName.getValue();
    	String givenName = theGivenName != null ? theGivenName.getValue() : null;
    	String sql = "";
    	

    	Object sqlParams[] = null;
    	if (givenName == null){
    		sql = "SELECT * FROM vwDohmpiBaby WHERE last_name = ?";
    		sqlParams = new Object[] { familyName };
    	} else {
    		sql = "SELECT * FROM vwDohmpiBaby WHERE first_name = ? AND last_name = ?";
    		sqlParams = new Object[] { givenName, familyName };
    	}
    	
    	List<Patient> patients = getPatients(sql, sqlParams);
        	
        return patients;
    }
    
    private List<Patient> getPatients(String sql, Object [] sqlParams){
    	
    	List<Patient> patients = new ArrayList<Patient>();
    	
    	jdbcTemplate = FhirServerConfig.getJdbcTemplate();
    	List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, sqlParams);

    	for (Map<String, Object> row : rows) {
    		Patient patient = new Patient();
    		patient.addIdentifier()
			   .setSystem("https://health.utah.gov/cshcn/programs/ehdi.html")
			   .setValue(row.get("brn").toString());
    		patient.addName()
			.setFamily((String)row.get("last_name"))
			.addGiven((String)row.get("first_name"));
    		if (row.get("gender").toString().charAt(0) == 'M')
    			patient.setGender(AdministrativeGender.MALE);
    		else
    			patient.setGender(AdministrativeGender.FEMALE);
    		patient.setId(new IdType("Patient", (String)row.get("id")));
    		
    		
    		//Add phone
    		if (row.get("phone_status_code") != null) {
	    		ContactPoint homeContactPhone = new ContactPoint();
	        	homeContactPhone.setSystem(ContactPointSystem.PHONE);
	        	homeContactPhone.setValue((String)row.get("ph_number"));
	        	homeContactPhone.setUse(ContactPointUse.HOME);
	        	patient.addTelecom(homeContactPhone);
    		}
    		
    		if (row.get("address_use") != null) {
	    		Address address = new Address();
	    		address.addLine((String)row.get("line_1"));
	    		address.addLine((String)row.get("line_2"));
	    		address.addLine((String)row.get("line_3"));
	    		address.setCity((String)row.get("city"));
	    		address.setState((String)row.get("state"));
	    		address.setPostalCode((String)row.get("zip"));    		
	    		patient.addAddress(address);
    		}
    		
    		
    		patients.add(patient);
    	}
    	
        return patients;
    }
    
}