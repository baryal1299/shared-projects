package utah.ehdi.fhir.ltf.providers;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import org.springframework.jdbc.core.JdbcTemplate;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.dstu3.model.Observation.ObservationStatus;

import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.param.TokenParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import utah.ehdi.fhir.ltf.FhirServerConfig;

public class RestfulObservationResourceProvider implements IResourceProvider {
	/**
     * The getResourceType method comes from IResourceProvider, and must
     * be overridden to indicate what type of resource this provider
     * supplies.
     */
	    
	private JdbcTemplate jdbcTemplate;
    
    public Class<Observation> getResourceType() {
        return Observation.class;
    }
     
    /**
     * The "@Read" annotation indicates that this method supports the
     * read operation. Read operations should return a single resource
     * instance.
     *
     * @param theId
     *    The read operation takes one parameter, which must be of type
     *    IdType and must be annotated with the "@Read.IdParam" annotation.
     * @return
     *    Returns a resource matching this identifier, or null if none exists.
     */
    
    @Read()
    public Observation getResourceById(@IdParam IdType theId) {
    	
    	String sql = "SELECT * FROM mcr WHERE mcr_id = ?";
    	
    	System.out.println(theId.getValue());
    	Object sqlParams[] = new Object[] { theId.getIdPart() };
    	    	
    	List<Observation> observations = getObservations(sql, sqlParams);
    	
    	if (observations.isEmpty()){
    		return null;
    	}else{
    		return observations.get(0);
    	}
        
    }
 
    /**
     * The "@Search" annotation indicates that this method supports the
     * search operation. You may have many different method annotated with
     * this annotation, to support many different search criteria. This
     * example searches by family name.
     *
     * @param theFamilyName
     *    This operation takes one parameter which is the search criteria. It is
     *    annotated with the "@Required" annotation. This annotation takes one argument,
     *    a string containing the name of the search criteria. The datatype here
     *    is StringParam, but there are other possible parameter types depending on the
     *    specific search criteria.
     * @return
     *    This method returns a list of Patients. This list may contain multiple
     *    matching resources, or it may also be empty.
     */
    
    @Search()
    public List<Observation> searchByPatientIdentifier(@RequiredParam(name=Patient.SP_IDENTIFIER) TokenParam patientIdentifer) {
    	
    	String patientId = patientIdentifer.getValue();    	
    	String sql = "SELECT * FROM mcr WHERE baby_id = ?";
    	Object sqlParams[] = new Object[] { patientId };
    	
    	List<Observation> observations = getObservations(sql, sqlParams);
        	
        return observations;
    }
    
    @Search()
    public List<Observation> searchByPatientName(@RequiredParam(name=Patient.SP_FAMILY) StringParam theFamilyName,
			  									 @RequiredParam(name=Patient.SP_GIVEN)  StringParam theGivenName) {
    	
    	String familyName = theFamilyName.getValue();
    	String givenName = theGivenName.getValue();
    	
    	String sql = "SELECT m.* FROM mcr m, vwDohmpiBaby v WHERE m.baby_id = v.id AND v.first_name = ? AND v.last_name = ?";
    	Object sqlParams[] = new Object[] { givenName, familyName };
    	
    	List<Observation> observations = getObservations(sql, sqlParams);
        	
        return observations;
    }
    
    private List<Observation> getObservations(String sql, Object [] sqlParams){
    	final String leftEarLoincCode = "54108-6";
    	final String leftEarDisplayName = "Newborn hearing screen of Ear - left";
    	final String rightEarLoincCode = "54109-4";
    	final String rightEarDisplayName = "Newborn hearing screen of Ear - right";
    	final String systemName = "http://loinc.org";
    	
    	List<Observation> observations = new ArrayList<Observation>();
    	
    	jdbcTemplate = FhirServerConfig.getJdbcTemplate();
    	List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, sqlParams);
    	
    	Patient patient = null;
    	
    	if (rows.isEmpty() == false){
	    	IdType patientIdType = new IdType();
	    	patientIdType.setValue(rows.get(0).get("baby_id").toString());
	    	
	    	patient = new RestfulPatientResourceProvider().getResourceById(patientIdType);
    	}
    	

    	for (Map<String, Object> row : rows) {
    		Observation observation = new Observation();
    		observation.setId(new IdType("Observation", row.get("mcr_id").toString()));
    		
    		if (row.get("MCREar") != null){
	    		if (row.get("MCREar").toString().charAt(0) == 'L') {
	    			observation.getCode().addCoding()
	    	     	   .setSystem(systemName)
	    	     	   .setCode(leftEarLoincCode)
	    	     	   .setDisplay(leftEarDisplayName);
	    		}
	    		else{
	    			observation.getCode().addCoding()
		 	     	   .setSystem(systemName)
		 	     	   .setCode(rightEarLoincCode)
		 	     	   .setDisplay(rightEarDisplayName);
	    		}
    		}
    		
    		observation.setSubject(new Reference(patient));
    		observation.setStatus(ObservationStatus.FINAL);
    		if(row.get("TestDate") != null){
        		DateTimeType effectiveDate = new DateTimeType(row.get("TestDate").toString());
        		observation.setEffective(effectiveDate);
    		}

    		observation.setValue(
    		    	   new Quantity()
    		    	      .setUnit((String)row.get("TestResult"))
    		    	      .setSystem((String)row.get("UsedMethod"))
    		    	      .setCode((String)row.get("TestType")));
    		
    		observations.add(observation);
    	}
    	
        return observations;
    }
    
}