package utah.ehdi.fhir.ltf;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement()
@EnableJpaRepositories("utah.ehdi.fhir.ltf.repository")
public class FhirServerConfig {
	
	@Autowired
	DataSource dataSource;
	
	static JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
	
	@Bean
	public static JdbcTemplate getJdbcTemplate(){
		return jdbcTemplate;
	}
	
	
	@Bean(destroyMethod = "close")
	public static DataSource dataSource() {
		BasicDataSource retVal = new BasicDataSource();
		retVal.setDriverClassName("com.mysql.cj.jdbc.Driver");
		String db_host = System.getenv("DB_HOST");
		String db_port = System.getenv("DB_PORT");
		String db_user = System.getenv("DB_USER");
		String db_password = System.getenv("DB_PASSWORD");
		String db_name = System.getenv("DB_NAME");
		String db_timezone = System.getenv("DB_TIMEZONE");
		String db_url = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db_name + "?serverTimezone=" + db_timezone;
		//retVal.setUrl("jdbc:mysql://mysql:3306/hitrack_swizzle?serverTimezone=CST");
		retVal.setUrl(db_url);
		retVal.setUsername(db_user);
		retVal.setPassword(db_password);
		return retVal;
	}
	

	@Bean()
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean retVal = new LocalContainerEntityManagerFactoryBean();
		retVal.setPersistenceUnitName("UTAH_EHDI_PU");
		retVal.setJpaVendorAdapter(getJpaVendorAdapter());
		retVal.setDataSource(dataSource());
		retVal.setPackagesToScan("utah.ehdi.fhir.ltf.models");
		retVal.setPersistenceProvider(new HibernatePersistenceProvider());
		retVal.setJpaProperties(jpaProperties());
		return retVal;
	}

	@Bean
	public JpaVendorAdapter getJpaVendorAdapter() {
	    JpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
	    return adapter;
	}
	
	private Properties jpaProperties() {
		Properties extraProperties = new Properties();
		extraProperties.put("hibernate.dialect", org.hibernate.dialect.MySQL5Dialect.class.getName());
		return extraProperties;
	}

	@Bean()
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager retVal = new JpaTransactionManager();
		retVal.setEntityManagerFactory(entityManagerFactory);
		return retVal;
	}

}
