package utah.ehdi.fhir.ltf.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="vwDohmpiBaby")
public class VwDohmpiBaby {
    @Id
    private String id;
    private String brn;
    
    @Column(name = "birthDatetime")
    private Date birth_datetime;
    
    @Column(name = "birthingFacilityId")
    private String birthing_facility_id;
    private Character gender;
    
    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "middle_names")
    private String middleName;
    
    @Column(name = "last_name")
    private String lastName;
    
    private String suffix;
    private Short weight;
    private Character multiple;
    
    @Column(name = "birthOrder")
    private Short birth_order;
    
    @Column(name = "stateFileNumber")
    private String state_file_number;
    
    @Column(name = "phoneStatusCode")
    private Character phone_status_code;
    
    @Column(name = "phNumber")
    private String ph_number;
    
    @Column(name = "addressUse")
    private Character address_use;
    
    @Column(name = "line1")
    private String line_1;
    
    @Column(name = "line2")
    private String line_2;
    
    @Column(name = "line3")
    private String line_3;
    
    private String city;
    private String county;
    private String state;
    private String zip;
    private String zip4;
    
    @Column(name = "CASScertified")
    private String CASS_certified;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrn() {
        return brn;
    }

    public void setBrn(String brn) {
        this.brn = brn;
    }

    public Date getBirthDatetime() {
        return birth_datetime;
    }

    public void setBirth_datetime(Date birthDatetime) {
        this.birth_datetime = birthDatetime;
    }

    public String getBirthingFacilityId() {
        return birthing_facility_id;
    }

    public void setBirthingFacilityId(String birthingFacilityId) {
        this.birthing_facility_id = birthingFacilityId;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddle_names(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLast_name(String lastName) {
        this.lastName = lastName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Short getWeight() {
        return weight;
    }

    public void setWeight(Short weight) {
        this.weight = weight;
    }

    public Character getMultiple() {
        return multiple;
    }

    public void setMultiple(Character multiple) {
        this.multiple = multiple;
    }

    public Short getBirth_order() {
        return birth_order;
    }

    public void setBirth_order(Short birthOrder) {
        this.birth_order = birthOrder;
    }

    public String getStateFileNumber() {
        return state_file_number;
    }

    public void setState_file_number(String stateFileNumber) {
        this.state_file_number = stateFileNumber;
    }

    public Character getPhoneStatusCode() {
        return phone_status_code;
    }

    public void setPhone_status_code(Character phoneStatusCode) {
        this.phone_status_code = phoneStatusCode;
    }

    public String getPhNumber() {
        return ph_number;
    }

    public void setPh_number(String phNumber) {
        this.ph_number = phNumber;
    }

    public Character getAddressUse() {
        return address_use;
    }

    public void setAddress_use(Character addressUse) {
        this.address_use = addressUse;
    }

    public String getLine1() {
        return line_1;
    }

    public void setLine_1(String line1) {
        this.line_1 = line1;
    }

    public String getLine2() {
        return line_2;
    }

    public void setLine_2(String line2) {
        this.line_2 = line2;
    }

    public String getLine3() {
        return line_3;
    }

    public void setLine_3(String line3) {
        this.line_3 = line3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getZip4() {
        return zip4;
    }

    public void setZip4(String zip4) {
        this.zip4 = zip4;
    }

    public String getCASScertified() {
        return CASS_certified;
    }

    public void setCASS_certified(String CASScertified) {
        this.CASS_certified = CASScertified;
    }
}
