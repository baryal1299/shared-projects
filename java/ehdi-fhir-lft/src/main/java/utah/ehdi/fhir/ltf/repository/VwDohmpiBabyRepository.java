package utah.ehdi.fhir.ltf.repository;

import utah.ehdi.fhir.ltf.models.VwDohmpiBaby;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface VwDohmpiBabyRepository extends CrudRepository<VwDohmpiBaby, String> {
	List<VwDohmpiBaby> findByFirstName(String firstName);
	List<VwDohmpiBaby> findByLastName(String lastName);
}
