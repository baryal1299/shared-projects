# TO DO List

## Refer to <http://cs6440.gatech.edu/deliverable-5/>

- Finalize catalog.pdf
- Finalize special instructions
- COMPLETED - Add final Gannt chart
- Add any other research used in the project as documentation or links
- Add any other relevant files
- Submit commit ID in a documentand submit it as a PDF called "Final Project - East Coast FHIR.pdf" in Canvas
- Check VM and make any necessary updates
- Add test cases to Final Delivery directory