
// Prevent Page refresh on button Click and reset form errors
$("button").click(function(event) {
    event.preventDefault();
});


var table = null;

function _get(path,data,callback){
    var url = "/ehdi-fhir-ltf/baseDstu3"+path;
    $(".data_error").html("");
    $.ajax({
        url: url,
        method: "GET",
        data: data,
        success: callback,
        error: function(xhr){
            console.log("Error Calling API")
            $(".data_error").html("No Matching Patient Found!");
        }
    });
}

function _print_json(data){
    $("pre.debug").text(JSON.stringify(data));
}

function search_for_patient(params,callback){
    var url = "/Patient"

    if(params.identifier){
        url += "/"+params.identifier;
        params = {}
    }

    _get(url, params, function (data) {
        // console.log(data);
        if (data.total === 0) {
            $(".data_error").html("No Matching Patient Found!")
        }else {
            if (data.total > 1) {
                patient_selection_table(data)
            }else if(data.total == 1){

                show_observation(data.entry[0].resource)
            }else{
                show_observation(data)
            }
        }
    });
}



function load_mrn(id){
    $("#mrn").val(id);
    $("#search_mrn_btn").click();

}

function patient_contact(resource){

    return patient_address(resource)
        + '<br/>'
        + patient_phone(resource)
}

function patient_address(resource){
    if('address' in resource){
        var address = resource.address[0]
        return address.line[0] + ', <br/>'+address.city+', '+address.state+' '+address.postalCode
    }else{
        return "";
    }
}

function patient_name(resource){
    var name = resource.name[0]
    var gender = resource.gender.charAt(0).toUpperCase();
    return name.given[0] + ' ' + name.family + ' ('+gender+') '
}

function patient_phone(resource){
    if('telecom' in resource) {
        var telecom = resource.telecom[0]
        return telecom.value + ' (' + telecom.use + ')'
    }else{
        return "";
    }
}


function _latest_status(observations){
    var latest_status = {"date":null,"status":null}
    observations.entry.map( function(entry){
        var _date = new Date(entry.resource.effectiveDateTime);
        if (latest_status.date == null || _date > new Date(latest_status.date)  ){
            latest_status.date = entry.resource.effectiveDateTime;
            latest_status.status = entry.resource.valueQuantity.unit.toLowerCase();
        }
    });

    return latest_status;
}

function final_hearing_status(observations){
    var latest_status = _latest_status(observations);
    if(latest_status.status == "pass"){
        return "<span class='alert-success p-1'>Passed: No Letter to be printed!</span>";
    }else{
        return "<span class='alert-danger p-1'>Not Passed: Please print the letter.</span>";
    }
}



function show_observation(data){
    var container = $("#observation");

    container.append($("<h4>",{'text':'Patient Info'}));

    var info_table = $("<table>",{"class":"table table-sm table-bordered table-observation"})
    info_table.append("<tr><th>Name</th><td>"+patient_name(data)+"</td></tr>")
    info_table.append("<tr><th>Baby Id</th><td>"+data.id+"</td></tr>")
    info_table.append("<tr><th>Address</th><td>"+patient_address(data)+"</td></tr>")
    info_table.append("<tr><th>Phone</th><td>"+patient_phone(data)+"</td></tr>")

   container.append(info_table);
    container.append($("<br>"));
   container.append($("<h4>",{'text':'Observations'}));

    _get("/Observation", {"identifier":data.id}, function (observation) {
        if (data.total === 0) {
            $(".data_error").html("No Matching Observations Found!")
        }else{
            info_table.append("<tr><th>Hearing Status</th><td>"+final_hearing_status(observation)+"</td></tr>")
            observation_table(observation, function(_table){
                container.append(_table);
            });

        }
    });

}



function init_datatable(table){
    var t = table.DataTable({
        "responsive": true,
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'desc' ]]
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
}

function observation_table(observation,callback){
    var table = $("<table>", {'class':'table table-sm table-bordered table-hover'})
    var thead = $('<thead>', {'class':'thead-dark'});

    thead.append("<tr><th>#</th><th>Test Date</th><th>LOINC Code</th><th>Used Method</th><th>Test Result</th><th>Status</th></tr>");
    table.append(thead);
    var tbody = $('<tbody>');
    observation.entry.forEach(function(item) {
        var tr = observation_table_row(item.resource);
        tbody.append(tr);
    });
    table.append(tbody);
    init_datatable(table);
    callback(table)
}

function observation_code(resource){
    if(resource.code == undefined ){
        return "";
    }

    var code = resource.code.coding[0];
    return code.code +" "+code.display
}

function observation_value(resource){
    var value = resource.valueQuantity
    return value.unit
}

function observation_used_method(resource){
    var value = resource.valueQuantity
    return value.system
}


function observation_table_row(resource){
    var tr = $('<tr>');
    tr.append('<td></td>');

    tr.append($('<td>').append(resource.effectiveDateTime));

    tr.append($('<td>').append(observation_code(resource)));

    tr.append($('<td>').append(observation_used_method(resource)));

    tr.append($('<td>').append(observation_value(resource)));

    tr.append($('<td>').append(resource.status));

    return tr;
}



function patient_selection_table(data){
    var container = $('#search_table');
    container.html("");
    var table = $('<table>', {'class':'table table-sm table-hover'});
    var thead = $('<thead>', {'class':'thead-dark'});
    thead.append("<tr><th>#</th><th>Patient Name</th><th>Contact Information</th></tr>");
    table.append(thead);
    var tbody = $('<tbody>');
    data.entry.forEach(function(item) {
        var tr = patient_selection_table_row(item.resource);
        tbody.append(tr);
    });
    table.append(tbody);
    init_datatable(table)
    container.append(table);
}

function patient_selection_table_row(resource){
    var tr = $('<tr>');
    tr.append('<td></td>');


    var link = $("<a>", {
        'href': '#',
        'onclick':'load_mrn("'+resource.id+'")',
        'class': 'btn btn-link btn-sm',
        'html': patient_name(resource)
    })

    tr.append($('<td>').append(link));

    var contacts = $("<span>",{
        "class":"contacts",
        "html": patient_contact(resource)
    });

    tr.append($('<td>').append(contacts));

    return tr;
}


function search_by_name(form){
    var first_name = $(form).find("#first_name").val()
    var last_name = $(form).find("#last_name").val()

    clean_html();
    // last_name = "Junsable"

    if (last_name.length === 0){
        $(form).find(".form_error").html("* Last Name is required!")
    }else {
        params = {}
        if (first_name.length !== 0){
            params['given'] = first_name
        }

        if (last_name.length !== 0){
            params['family'] = last_name
        }
        search_for_patient(params)
    }
}

function search_by_mrn(form){
    var mrn = $(form).find("#mrn").val()

    clean_html();

    if (mrn.length === 0 ){
        $(form).find(".form_error").html("* MRN# is required.")
    }else {
        search_for_patient({'identifier':mrn})
    }
}

function clean_html(){
    $(".form_error").html("");
    $("pre.debug").html("");
    $('#search_table').html("");
    $("#observation").html("");
}

$(document).ready(function(){
    $("#example").DataTable();


});